package dam.edusoft.imagenesuploaddownload;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.Hashtable;
import java.util.Map;

/* Servidor php para subir la foto usando este código
<?php
file_put_contents($_POST['fileName'], base64_decode($_POST['fileByte']));
?>
 */

public class MainActivity extends AppCompatActivity {

    /*private String HOST = "http://localhost/";*/
    /*private String HOST = "http://kompassaviacion.com/"; // si queremos*/
    //-> del cifredo datos.jpg
    //http://kompassaviacion.com/datos.jpg
    public static final int PICK_IMAGE = 1;

    private String HOST = "http://192.168.1.103/android/"; // si queremos conectar con un servidor local

    private static final String TAG = "MainActivity";

    private EditText editTextFileName;
    private  ImageView imageViewSupletoria;




    private Bitmap bitmap; //contiene información de una imagen
    private ImageView imgFoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageViewSupletoria = findViewById(R.id.ivSupletoria);
        imgFoto = findViewById(R.id.imageViewFoto);
        editTextFileName = findViewById(R.id.txtFileName);

        findViewById(R.id.buttonGaleria).setOnClickListener(v -> selectFromGallery());
        findViewById(R.id.buttonDownload).setOnClickListener(v -> download());
        findViewById(R.id.buttonUpload).setOnClickListener(v -> upload());
    }

    /**
     *  Método para subir una imagen
     */
    private void upload() {
        //verifico que el usuario haya escrito algún mensaje
        if(editTextFileName.getText().toString().length() == 0){
            Toast.makeText(this, "Falta nombre de archivo", Toast.LENGTH_LONG).show();
            return;
        }

        String strURL = HOST + "upload-image.php"; //esta petición carga la imagen en nuestro host

        final ProgressDialog loading = ProgressDialog.show(this, "subiendo...", "Espere por favor", false,false);

        StringRequest stringRequest = new StringRequest(
                Request.Method.POST //siempre para array de bytes
                ,strURL //hacia donde lo mandamos
                , s -> { //lo que ocurre si  ha ido bien
            loading.dismiss();
            Toast.makeText(this, "Subida imagen con éxito", Toast.LENGTH_SHORT).show();
        }, VolleyError ->{ //si va mal
            loading.dismiss();
            Toast.makeText(this, "Subida imagen con éxito", Toast.LENGTH_SHORT).show();
        }
        ){
            public Map<String,String> getParams(){//los parámetros necesarios para el envío subir la imagen
                Hashtable<String,String> params = new Hashtable<>();
                params.put("fileName", editTextFileName.getText().toString()); //nombre de la foto que subo
                params.put("fileByte", getStringImage(bitmap)); //nombre de cómo se guardará la foto

                return params;
            }
        };



        RequestQueue requestQueue = Volley.newRequestQueue(this); //generamos una cola
        requestQueue.add(stringRequest); //añadimos algo a la cola de envíos (sólo enviaremos una)
    }

    /**
     * Convierte un bitmap en un String
     * @param bitmap
     * @return
     */
    private String getStringImage(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,baos); //comprimimos sin comprimir (nos quedamos con el 100%)
        byte [] imageBytes = baos.toByteArray();


        return Base64.encodeToString(imageBytes, Base64.DEFAULT);

    }

    /** Método para descargar imagen del host
     *
     */
    private void download() {

        if (editTextFileName.getText().toString().length() == 0){
            Toast.makeText(getApplicationContext(), "Falta nombre archivo", Toast.LENGTH_LONG).show();
            return;
        }

        imgFoto.setImageDrawable(null);
        Glide
                .with(getApplicationContext())
                .load(HOST  + editTextFileName.getText().toString())
                .apply(RequestOptions.circleCropTransform())
                .into(imgFoto);
        bitmap = imgFoto.getDrawingCache();



    }

    //Métodos para seleccionar de la gelería
    private void selectFromGallery() {


        final Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, 1);


    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: " +  resultCode);
        if (resultCode == RESULT_OK) {
            try {
                Uri path = data.getData();
                bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(path));
                // imagen.setImageBitmap(bit); //alternativa
                imgFoto.setImageURI(path);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else{
            Log.d(TAG, "onActivityResult: WTF?");
        }
    }
}
